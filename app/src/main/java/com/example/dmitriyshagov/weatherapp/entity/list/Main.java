package com.example.dmitriyshagov.weatherapp.entity.list;


import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("temp")
    private double temp;

    @SerializedName("temp_max")
    private double temp_max;

    @SerializedName("temp_min")
    private double temp_min;

    @SerializedName("pressure")
    private double pressure;

    @SerializedName("sea_level")
    private double sea_level;

    @SerializedName("grnd_level")
    private double grnd_level;

    @SerializedName("humidity")
    private double humidity;

    @SerializedName("temp_kf")
    private double temp_kf;






    public double getTemp() {
        return temp;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public double getPressure() {
        return pressure;
    }

    public double getSea_level() {
        return sea_level;
    }

    public double getGrnd_level() {
        return grnd_level;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getTemp_kf() {
        return temp_kf;
    }
}
