package com.example.dmitriyshagov.weatherapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dmitriyshagov.weatherapp.adapter.WeatherArrayAdapter;
import com.example.dmitriyshagov.weatherapp.api.ApiClient;
import com.example.dmitriyshagov.weatherapp.api.ApiInterface;
import com.example.dmitriyshagov.weatherapp.entity.Weather;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isInternetEnabled()) {
            if (isGpsEnabled()) {
                getWeather();
            } else {
                shopGpsAlertDialog();
            }
        } else showInternetAlertDialog();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getWeather();
                }

        }
    }

    private void getWeather() {
        createProgressDialog();
        final LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        String bestProvider = locationManager.getBestProvider(new Criteria(), true);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
        } else {

            Location location = locationManager.getLastKnownLocation(bestProvider);
            if (location != null) {

                createWeatherRequest(location);

            } else {
                locationManager.requestLocationUpdates(bestProvider, 1000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
                        }else {
                            locationManager.removeUpdates(this);
                            createWeatherRequest(location);
                        }
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }
                });

            }
        }



    }

    private void createWeatherRequest(Location location){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.d("retrofit", "location " + String.valueOf(location.getLatitude()) + " " + String.valueOf(location.getLongitude()));
        Call<Weather> call = apiService.getWeather(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), Constant.APP_ID);
        call.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                initControlsAndShow(response.body());
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {

            }
        });

    }

    private void initControlsAndShow(Weather weather){

        TextView textViewCountry = (TextView)findViewById(R.id.textViewCountry);
        textViewCountry.setText(weather.getCity().getCountry());

        TextView textViewCity = (TextView)findViewById(R.id.textViewCity);
        textViewCity.setText(weather.getCity().getName());

        TextView textViewCurrentWeather = (TextView)findViewById(R.id.textViewCurrentWeather);
        String dim = weather.getList().get(0).getWeather().get(0).getDescription() + " " + String.valueOf(Math.round(weather.getList().get(0).getMain().getTemp() - 272.15)) + "°C";
        textViewCurrentWeather.setText(dim);

        ImageView imageViewMainIcon = (ImageView)findViewById(R.id.imageViewMainIcon);

        if (weather.getList().get(0).getWeather().get(0).getDescription().contains("rain")){
            imageViewMainIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.weather_rainy, null));
        } else if (weather.getList().get(0).getWeather().get(0).getDescription().contains("snow")){
            imageViewMainIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.weather_snowy, null));
        }else if (weather.getList().get(0).getWeather().get(0).getDescription().contains("wind")){
            imageViewMainIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.weather_windy, null));
        }else if (weather.getList().get(0).getWeather().get(0).getDescription().contains("sun")){
            imageViewMainIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.white_balance_sunny, null));
        }

        ListView listViewDailyWeather = (ListView)findViewById(R.id.listViewDailyWeather);
        WeatherArrayAdapter arrayAdapter = new WeatherArrayAdapter(this,R.layout.item_daily_weather, weather.getList());
        listViewDailyWeather.setAdapter(arrayAdapter);

        if (progressDialog != null){
            progressDialog.dismiss();
        }

    }




    private boolean isInternetEnabled() {

        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    private boolean isGpsEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    private void showInternetAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Внимание!");
        builder.setMessage("Отсутствует подключение к интернету. Проверьте подключение и нажмите ОК");
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (isInternetEnabled()) {
                    if (isGpsEnabled()) {
                        getWeather();
                    } else {
                        shopGpsAlertDialog();
                    }
                } else showInternetAlertDialog();
            }
        });
        builder.create().show();
    }

    private void shopGpsAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Внимание!");
        builder.setMessage("GPS отключен на Вашем устройстве. Включить?");
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        builder.create().show();

    }

    private void createProgressDialog(){

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Подождите, идет загрузка");
        progressDialog.show();

    }
}
