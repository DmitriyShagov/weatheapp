package com.example.dmitriyshagov.weatherapp.entity;


import com.google.gson.annotations.SerializedName;

public class City {

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("coord")
    private Coordinate coord;

    @SerializedName("country")
    private String country;

    @SerializedName("population")
    private long population;

    @SerializedName("sys")
    private Sys sys;

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private double message;

    @SerializedName("cnt")
    private long cnt;




    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Coordinate getCoord() {
        return coord;
    }

    public String getCountry() {
        return country;
    }

    public Sys getSys() {
        return sys;
    }

    public double getMessage() {
        return message;
    }

    public long getCnt() {
        return cnt;
    }
}
