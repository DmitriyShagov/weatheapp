package com.example.dmitriyshagov.weatherapp.entity.list;


import com.google.gson.annotations.SerializedName;

public class WeatherListItem {

    @SerializedName("id")
    private int id;

    @SerializedName("main")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("icon")
    private String icon;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
}
