package com.example.dmitriyshagov.weatherapp.entity;


import com.google.gson.annotations.SerializedName;

public class Coordinate {

    @SerializedName("lat")
    private double lat;

    @SerializedName("lon")
    private double lon;

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
