package com.example.dmitriyshagov.weatherapp.entity;


import com.example.dmitriyshagov.weatherapp.entity.list.Item;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Weather {

    @SerializedName("city")
    private City city;

    @SerializedName("list")
    private List<Item> list;

    public City getCity() {
        return city;
    }

    public List<Item> getList() {
        return list;
    }
}
