package com.example.dmitriyshagov.weatherapp.entity;


import com.google.gson.annotations.SerializedName;

public class Sys {

    @SerializedName("sys")
    private long population;

    public long getPopulation() {
        return population;
    }
}
