package com.example.dmitriyshagov.weatherapp.entity.list;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

    @SerializedName("dt")
    private long dt;

    @SerializedName("main")
    private Main main;

    @SerializedName("weather")
    private List<WeatherListItem> weather;

    @SerializedName("clouds")
    private Clouds clouds;

    @SerializedName("wind")
    private Wind wind;

    @SerializedName("rain")
    private Rain rain;

    @SerializedName("snow")
    private Snow snow;

    @SerializedName("sys")
    private Sys sys;

    @SerializedName("dt_txt")
    private String dt_txt;


    public long getDt() {
        return dt;
    }

    public Main getMain() {
        return main;
    }

    public List<WeatherListItem> getWeather() {
        return weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public Snow getSnow() {
        return snow;
    }

    public Rain getRain() {
        return rain;
    }

    public Sys getSys() {
        return sys;
    }

    public String getDt_txt() {
        return dt_txt;
    }
}
