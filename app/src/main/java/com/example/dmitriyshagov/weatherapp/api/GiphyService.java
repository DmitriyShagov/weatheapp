package com.example.dmitriyshagov.weatherapp.api;

import com.example.dmitriyshagov.weatherapp.entity.Weather;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface GiphyService {

    @GET("/data/2.5/forecast")
    Call<Weather> getWeather (@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String appid);



    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://api.openweathermap.org")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
