package com.example.dmitriyshagov.weatherapp.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dmitriyshagov.weatherapp.R;
import com.example.dmitriyshagov.weatherapp.entity.list.Item;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class WeatherArrayAdapter extends ArrayAdapter<Item> {

    private Context context;
    private int resource;
    private List<Item> list;

    public WeatherArrayAdapter(Context context, int resource, List<Item> list) {
        super(context, resource, list);
        this.context = context;
        this.resource = resource;
        this.list = list;
    }

    private class ViewHolder{

        TextView textViewDate, textViewWheatherDescription, textViewWeatherDim, textViewWind, textViewRain, textViewHumidity;
        ImageView imageViewDailyicon;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;
        ViewHolder viewHolder;

        if (root == null){

            root = LayoutInflater.from(context).inflate(resource, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textViewDate = (TextView)root.findViewById(R.id.textViewDate);
            viewHolder.textViewWheatherDescription = (TextView)root.findViewById(R.id.textViewWeatherDescription);
            viewHolder.textViewWeatherDim = (TextView)root.findViewById(R.id.textViewWeatherDim);
            viewHolder.imageViewDailyicon = (ImageView)root.findViewById(R.id.imageViewDailyIcon);
            viewHolder.textViewRain = (TextView)root.findViewById(R.id.textViewRain);
            viewHolder.textViewWind = (TextView)root.findViewById(R.id.textViewWind);
            viewHolder.textViewHumidity = (TextView)root.findViewById(R.id.textViewHumidity);

            root.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)root.getTag();

        }


        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(list.get(position).getDt_txt());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewHolder.textViewDate.setText(new SimpleDateFormat("dd.MM HH:mm").format(date));
        viewHolder.textViewWeatherDim.setText(String.valueOf(Math.round(list.get(position).getMain().getTemp() - 272.15)) + "°C");
        viewHolder.textViewWheatherDescription.setText(list.get(position).getWeather().get(0).getDescription());

        viewHolder.textViewWind.setText(String.valueOf(list.get(position).getWind().getSpeed())+"mph");
        viewHolder.textViewRain.setText(String.valueOf(list.get(position).getRain().getHhh()));
        viewHolder.textViewHumidity.setText(String.valueOf(list.get(position).getMain().getHumidity()));

        if (list.get(0).getWeather().get(0).getDescription().contains("rain")){
            viewHolder.imageViewDailyicon.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.drawable.weather_rainy, null));
        } else if (list.get(0).getWeather().get(0).getDescription().contains("snow")){
            viewHolder.imageViewDailyicon.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.drawable.weather_snowy, null));
        }else if (list.get(0).getWeather().get(0).getDescription().contains("wind")){
            viewHolder.imageViewDailyicon.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.drawable.weather_windy, null));
        }else if (list.get(0).getWeather().get(0).getDescription().contains("sun")){
            viewHolder.imageViewDailyicon.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.drawable.white_balance_sunny, null));
        }
        return root;


    }
}
